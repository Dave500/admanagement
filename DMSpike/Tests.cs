﻿using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;

namespace DMSpike{
    [TestClass]
    public class Tests
    {
        // look at http://stackoverflow.com/questions/15011291/asp-net-active-directory-search ?
        [TestMethod]
        public void GivenASearchForDomainAdminGroup_ShouldReturn81ADUsersInThatGroup(){
            var adSearcher = new ADSearcher();

            List<string> listOfUsersInDomainAdminGroup = adSearcher.GetAllUsersInGroup("Domain Admins");

            Assert.AreEqual(81, listOfUsersInDomainAdminGroup.Count());
        }
    }
}