﻿using System;
using System.Collections.Generic;
using System.DirectoryServices.AccountManagement;
using System.Linq;
using ADManagement;

namespace DMSpike
{
    public class ConsoleRunner
    {
        private static void Main()
        {
            var adSearcher = new ADSearcher();
            adSearcher.Run();
        }
    }

    public class ADSearcher
    {
        private PrincipalContext principalContext;

        public ADSearcher()
        {
            //principalContext = new PrincipalContext(ContextType.Domain, "qnrl.com", null, ContextOptions.Negotiate, @"qnrl\davidma", "*****");
            principalContext = new PrincipalContext(ContextType.Domain, "qnrl.com", null, ContextOptions.Negotiate);
        }

        public void Run()
        {
            //GetAllUsersAndAssociatedGroups();

            GetAllUsersTheirGroups();

            // GetAllGroupsAndTheirUsers();
        }

        public void GetAllUsersTheirGroups()
        {
            // List Users
            var userPrincipal = new UserPrincipal(principalContext);
            userPrincipal.Name = "*";
            userPrincipal.Surname = "Mateer";

            var principalSearcher = new PrincipalSearcher();
            principalSearcher.QueryFilter = userPrincipal;

            PrincipalSearchResult<Principal> results = principalSearcher.FindAll();
            //foreach (Principal p in results.OrderBy(x => x.Name))
            foreach (UserPrincipal p in results.OrderBy(x => x.Name))
            {
                Console.WriteLine("{0} : {1}", p.DisplayName, p.Description);
                // Finding Groups that the user belongs to
                PrincipalSearchResult<Principal> groups = p.GetGroups();
                foreach (var group in groups.OrderBy(x => x.Name))
                {
                    Console.WriteLine("{0}", @group.Name);
                }
            }
        }

        public ADUser GetUserByName(string name)
        {
             // List Users
            var userPrincipal = new UserPrincipal(principalContext);
            userPrincipal.Name = "*";
            userPrincipal.DisplayName = name;
            var user = new ADUser();

            var principalSearcher = new PrincipalSearcher();
            principalSearcher.QueryFilter = userPrincipal;

            PrincipalSearchResult<Principal> results = principalSearcher.FindAll();
            //foreach (Principal p in results.OrderBy(x => x.Name))
            foreach (UserPrincipal p in results.OrderBy(x => x.Name))
            {
                user.DisplayName = p.DisplayName;
                if (p.LastLogon != null) user.LastLogin = (DateTime)p.LastLogon;
                user.Login = p.SamAccountName;
            }

            return user;

        }

        public void GetAllGroupsAndTheirUsers()
        {
            var groupPrincipal = new GroupPrincipal(principalContext);
            groupPrincipal.Name = "*";

            var principalSearcher = new PrincipalSearcher();
            principalSearcher.QueryFilter = groupPrincipal;

            PrincipalSearchResult<Principal> results = principalSearcher.FindAll();
            foreach (GroupPrincipal p in results.OrderBy(x => x.Name))
            {
                Console.WriteLine("{0} : {1}", p.Name, p.Description);
                foreach (var user in p.Members.OrderBy(x => x.Name))
                {
                    Console.WriteLine("{0}", user.Name);
                }
            }
        }

        public List<string> GetAllUsersInGroup(string groupName)
        {
            var groupPrincipal = new GroupPrincipal(principalContext);
            groupPrincipal.Name = groupName;

            var groupPrincipalSearcher = new PrincipalSearcher();
            groupPrincipalSearcher.QueryFilter = groupPrincipal;

            PrincipalSearchResult<Principal> groupSearcherResults = groupPrincipalSearcher.FindAll();
            List<string> listOfUserNamesInGroup = new List<string>();
            foreach (GroupPrincipal groupPrincipalResult in groupSearcherResults.OrderBy(x => x.Name))
            {
                //Console.WriteLine("{0} : {1}", groupPrincipal.Name, groupPrincipal.Description);
                foreach (var userInGroup in groupPrincipalResult.Members.OrderBy(x => x.Name))
                {
                    //Console.WriteLine("{0}", userInGroup.Name);
                    listOfUserNamesInGroup.Add(userInGroup.Name);
                }
            }
            return listOfUserNamesInGroup;
        }
    }
}
