﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ADModel
{
    public class ADGroup
    {
        public string Name { get; set; }
        public List<ADUser> Users { get; set; }
    }
}
