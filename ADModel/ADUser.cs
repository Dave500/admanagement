﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace ADModel
{
    public class ADUser
    {
        public string Login { get; set; }
        public MailAddress PrimaryEmail { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string DisplayName { get; set; }
        public string JobTitle { get; set; }
        public string Initials { get; set; }
        public string Groups { get; set; }
        public DateTime LastLogin { get; set; }
    }
}
