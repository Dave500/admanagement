﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ADManagement;

namespace ADReporter
{
    public static class ADConnection
    {
        public static ADConnector GetConnector(string pwd)
        {

            //string Address = ConfigurationManager.AppSettings["ldap"];
            //string User = ConfigurationManager.AppSettings["usr"];
            // string Pwd = ConfigurationManager.AppSettings["pwd"];

            string Address = string.Concat( "LDAP://", Environment.UserDomainName);
            string User = Environment.UserName;

           

            ADConnector connector = new ADConnector(ADConnector.Port.LDAP_CONNECTION, Address, User, pwd );

            return connector;
        }
    }
}
