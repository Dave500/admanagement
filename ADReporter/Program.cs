﻿using ADManagement;
using System;
using System.Security.Permissions;

namespace ADReporter
{
    class Program
    {
        [PermissionSet(SecurityAction.Demand, Name = "FullTrust")]
        public static void Main()
        {
            string userName = Environment.UserName;

            Console.Write("Enter the password for {0}: ", userName);
            string pwd = Console.ReadLine();

            Console.Write("Enter Group Name or Partial Keyword: ");
            string keyWord = Console.ReadLine();

            Console.WriteLine("Getting Info: Please Wait");
            
            RunReport(pwd, keyWord);

            Console.WriteLine(@"Report Complete: Location C:\temp. GoodBye");

            Console.ReadLine();
         }

        private static void RunReport(string pwd, string keyWord)
        {
            ADConnector connector = ADConnection.GetConnector(pwd);
            var adManager = new ActiveDirectoryManagement(connector);

            // var groups = adManager.GetAllGroups3();

            var groups = adManager.GetADGroupsAndAssociatedADUsers(keyWord);

            CreateSpreadSheet css = new CreateSpreadSheet();
            
            bool reportDone = css.BuildCsv(groups, keyWord);
        }
    }
}
