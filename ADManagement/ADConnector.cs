﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ADManagement
{
    /// <summary>
    /// AD Connector is used to pass credentials to connect to ActiveDirectory    
    /// </summary>
    public class ADConnector
    {

        public enum Port { LDAP_CONNECTION = 389, ADGLOBAL_CATALOG = 3268 };

        private Port _port;
        private string _dnsAddress;
        private string _user;
        private string _pword;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="port">Connecting Port</param>
        /// <param name="address">Server to Connect to</param>
        /// <param name="user">User to connect</param>
        /// <param name="password"></param>
        public ADConnector(Port port, string address, string user, string password)
        {
            _port = port;
            _dnsAddress = address;
            _user = user;
            _pword = password;
        }

        /// <summary>
        /// Port to connect on
        /// </summary>
        public Port ConnectingPort
        {
            get { return _port; }
        }

        /// <summary>
        /// Server address to connect to
        /// </summary>
        public string DNSAddress
        {
            get { return _dnsAddress; }
        }

        public string Path
        {
            get { return _dnsAddress + ":" + (int)_port; }
        }

        /// <summary>
        /// User name to connect as
        /// </summary>
        internal string User
        {
            get { return _user; }
        }

        /// <summary>
        /// Password to authenticate with
        /// </summary>
        internal string PassWord
        {
            get { return _pword; }
        }
    }
}
