﻿using System;
using System.Collections.Generic;
using System.Net.Mail;
using System.DirectoryServices;
using System.DirectoryServices.AccountManagement;
using System.Linq;
using System.Text;
using ADModel;

namespace ADManagement
{
    /// <summary>
    /// LDAP method Pre .NET 3.5
    /// </summary>
    public class ActiveDirectoryManagement
    {
        private static string PRIMARY_MAIL = "mail";
        private static string INITIALS = "initials";
        private static string JOB_TITLE = "title";
        private static string FIRST_NAME = "givenName";
        private static string SURNAME = "sn";
        private static string LOGIN = "sAMAccountName";
        private static string DISPLAY = "displayname";
        private static string GROUP = "memberOf";
        private static string LASTLOGIN = "lastLogon";

        // Group props ??

        private static string USERS = "useraccountcontrol";
        private static string GIVENNAME = "givenName";
        private static string PATH = "adspath";
        private static string MEMBER = "member";
        private static string MEMBEROF = "memberOf";

        private ADConnector _connector;

        public ActiveDirectoryManagement(ADConnector connector)
        {
            _connector = connector;
        }

        public ADUser GetUserFromLogin(string login)
        {
            return GetUser(string.Concat("(", LOGIN, "=", login,  ")"));
        }

        public ADUser GetUserFromEmail(MailAddress email)
        {
            return GetUser(string.Concat("(",  PRIMARY_MAIL, "=", email.Address, ")"));
        }


        public ADUser GetUserFromName(string name)
        {
            return GetUser(string.Concat("(", DISPLAY, "=", name, ")"));
         }


        public ADUser GetUserFromGivenName(string name)
        {
            return GetUser(string.Concat("(", GIVENNAME, "=", name, ")"));
        }

        private ADUser GetUser(string filter)
        {
            ADUser user = new ADUser();
            
            DirectoryEntry adserver = null;
            DirectorySearcher searcher = null;
            adserver = new DirectoryEntry(_connector.Path, _connector.User, _connector.PassWord);
            searcher = new DirectorySearcher(adserver);

         //   searcher.Filter =  String.Concat("(&(objectClass=user)(objectCategory=person)(!userAccountControl:1.2.840.113556.1.4.803:=2)", filter, ")");
            searcher.Filter = String.Concat("(&(objectClass=user)(objectCategory=person)", filter , ")");
            
            searcher.SearchScope = SearchScope.Subtree;

            searcher.PropertiesToLoad.Add(PRIMARY_MAIL);
            searcher.PropertiesToLoad.Add(INITIALS);
            searcher.PropertiesToLoad.Add(JOB_TITLE);
            searcher.PropertiesToLoad.Add(FIRST_NAME);
            searcher.PropertiesToLoad.Add(SURNAME);
            searcher.PropertiesToLoad.Add(LOGIN);
            searcher.PropertiesToLoad.Add(DISPLAY);
            searcher.PropertiesToLoad.Add(LASTLOGIN);

            SearchResult result = searcher.FindOne();

            if (result != null)
            {
                string email = GetPropertyValue(result.Properties[PRIMARY_MAIL]);
                if (!string.IsNullOrEmpty(email)) user.PrimaryEmail = new MailAddress(email);

                user.Initials = GetPropertyValue(result.Properties[INITIALS]);
                user.Name = GetPropertyValue(result.Properties[FIRST_NAME]);
                user.Surname = GetPropertyValue(result.Properties[SURNAME]);
                user.Login = GetPropertyValue(result.Properties[LOGIN]);
                user.DisplayName = GetPropertyValue(result.Properties[DISPLAY]);
                user.JobTitle = GetPropertyValue(result.Properties[JOB_TITLE]);
                var ll = GetPropertyValue(result.Properties[LASTLOGIN]);
                DateTime dtLl = DateTime.FromFileTime(Convert.ToInt64(ll));
                if (dtLl > Convert.ToDateTime("01/01/1901")) user.LastLogin = dtLl;
            }

            return user;
        }

        private string GetPropertyValue(ResultPropertyValueCollection result)
        {
            if (result.Count > 0)
            {
                return result[0].ToString();
            }
            else
            {
                return null;
            }
        }

        public List<ADUser> GetAllUsers()
        {
            var users = new List<ADUser>();

            using (DirectoryEntry adserver = new DirectoryEntry(_connector.Path, _connector.User, _connector.PassWord))
            using (DirectorySearcher searcher = new DirectorySearcher(adserver))
            {
                searcher.Filter = "(&(objectClass=user))";
                searcher.SearchScope = SearchScope.Subtree;

                searcher.PropertiesToLoad.Add(PRIMARY_MAIL);
                searcher.PropertiesToLoad.Add(INITIALS);
                searcher.PropertiesToLoad.Add(JOB_TITLE);
                searcher.PropertiesToLoad.Add(FIRST_NAME);
                searcher.PropertiesToLoad.Add(SURNAME);
                searcher.PropertiesToLoad.Add(LOGIN);
                searcher.PropertiesToLoad.Add(DISPLAY);
                searcher.PropertiesToLoad.Add(GROUP);
                searcher.PropertiesToLoad.Add(LASTLOGIN);

                using (SearchResultCollection results = searcher.FindAll())
                {
                    foreach (SearchResult result in results)
                    {
                        var user = new ADUser();
                        string email = GetPropertyValue(result.Properties[PRIMARY_MAIL]);
                        if (!string.IsNullOrEmpty(email)) user.PrimaryEmail = new MailAddress(email);

                        user.Initials = GetPropertyValue(result.Properties[INITIALS]);
                        user.Name = GetPropertyValue(result.Properties[FIRST_NAME]);
                        user.Surname = GetPropertyValue(result.Properties[SURNAME]);
                        user.Login = GetPropertyValue(result.Properties[LOGIN]);
                        user.DisplayName = GetPropertyValue(result.Properties[DISPLAY]);
                        user.JobTitle = GetPropertyValue(result.Properties[JOB_TITLE]);

                        var grps = GetPropertyValue(result.Properties[GROUP]);

                        if (grps != null)
                        {
                            StringBuilder groupsList = new StringBuilder();
                            int groupCount = result.Properties[GROUP].Count;

                            // Just take CN Common Name from Group
                            for (int counter = 0; counter < groupCount; counter++)
                            {
                                List<string> cnValues = new List<string>();

                                string groupStr = (string) result.Properties[GROUP][counter];
                                string[] split = groupStr.Split(',');

                                foreach (string pair in split)
                                {
                                    string[] keyValue = pair.Split('=');

                                    if (keyValue[0] == "CN")
                                    {
                                        groupsList.Append(keyValue[1]);
                                        groupsList.Append("|");
                                    }
                                }
                            }
                            user.Groups = groupsList.ToString();

                        }

                        var ll = GetPropertyValue(result.Properties[LASTLOGIN]);
                        DateTime dtLl = DateTime.FromFileTime(Convert.ToInt64(ll));
                        if (dtLl > Convert.ToDateTime("01/01/1901")) user.LastLogin = dtLl;
                            

                        users.Add(user);
                    }
                }
            }

            return users;
        }

        public List<ADGroup> GetAllGroups()
        {
            var groups = new List<ADGroup>();

            using (DirectoryEntry adserver = new DirectoryEntry(_connector.Path, _connector.User, _connector.PassWord))
            using (DirectorySearcher searcher = new DirectorySearcher(adserver))
            {
                searcher.Filter = "(&(objectClass=group))";
                searcher.SearchScope = SearchScope.Subtree;

                searcher.PropertiesToLoad.Add(GROUP);
                searcher.PropertiesToLoad.Add(PATH);
                searcher.PropertiesToLoad.Add(MEMBER);
                searcher.PropertiesToLoad.Add(MEMBEROF);

                using (SearchResultCollection results = searcher.FindAll())
                {
                    foreach (SearchResult result in results)
                    {
                        var pth = GetPropertyValue(result.Properties[PATH]);
                        var grp = GetPropertyValue(result.Properties[GROUP]);
                        var mem = GetPropertyValue(result.Properties[MEMBER]);
                        var mof = GetPropertyValue(result.Properties[MEMBEROF]);
                    }
                }

            }
       
            return null;
            

        }

        public List<ADGroup> GetAllGroups2()
        {

            var groups = new List<ADGroup>();
             
             using (DirectoryEntry adserver = new DirectoryEntry(_connector.Path, _connector.User, _connector.PassWord))
             using (DirectorySearcher searcher = new DirectorySearcher(adserver))
             {
                 searcher.Filter = "(&(objectClass=group))";
                 searcher.SearchScope = SearchScope.Subtree;

                 searcher.PropertiesToLoad.Add(GROUP);
                 searcher.PropertiesToLoad.Add(USERS);



                 
                 
                 using (SearchResultCollection results = searcher.FindAll())
                  {
                    foreach (SearchResult result in results)
                    {

                        var group = new ADGroup();

                        var grp = GetPropertyValue(result.Properties[GROUP]);
                        var usrs = GetPropertyValue(result.Properties[USERS]);

                        if (grp != null)
                        {
                         
                                string[] split = grp.Split(',');

                                foreach (string pair in split)
                                {
                                    string[] keyValue = pair.Split('=');

                                    if (keyValue[0] == "CN")
                                    {
                                        group.Name = keyValue[1];
                                    }
                                }
                            }

                        if (group.Name != null)
                        {
                            bool alreadyExists = groups.Any(g => g.Name == group.Name);

                            if (! alreadyExists)
                            {
                                groups.Add(group);
                            }
                            
                        }
                           
                        }


                    }
                }


            foreach (var group in groups)
            {

                using (PrincipalContext ctx = new PrincipalContext(ContextType.Domain))
                {

                    using (GroupPrincipal g1 = GroupPrincipal.FindByIdentity(ctx, group.Name))
                    {

                        if (g1 != null)
                        {
                            foreach (Principal p in g1.GetMembers())
                            {

                                UserPrincipal theUser = p as UserPrincipal;
                            }
                        }


                    }
                    
                }
                
                
            }

            return groups;

        }

        public List<ADGroup> GetAllGroups3()
        {
            var groupList = new List<ADGroup>();
            var userList = GetAllUsers();

            foreach (var user in userList)
            {

                if (user.Groups != null)
                {
                    string[] groups = user.Groups.Split('|');

                    foreach (var group in groups)
                    {
                        if (group != null)
                        {
                            bool groupExists = groupList.Any(g => g.Name == group);

                            if (!groupExists)
                            {
                                var adGroup = new ADGroup();
                                adGroup.Name = group;

                                var nameOrLogin = user.Name ?? user.Login;

                                adGroup.Users = new List<ADUser>();

                                adGroup.Users.Add(user);

                                groupList.Add(adGroup);
                            }
                            else
                            {   
                                ADGroup gr = groupList.FirstOrDefault(g => g.Name == group);
                                int indexOf = groupList.IndexOf(gr);

                                var fullName = string.Concat(user.Name, " ", user.Surname);

                                var nameOrLogin = fullName ?? user.Login;

                                bool userExists = gr.Users.Any(u => u.Name == nameOrLogin);

                                if (!userExists)
                                {
                                    gr.Users.Add(user);

                                groupList.RemoveAt(indexOf);

                                groupList.Add(gr);
                                }

                                
                            }
                        }
                    }

                }
            }

            return groupList;
        }

        // Get AD Group and Associated users using wildcard match on group name
        public List<ADGroup> GetADGroupsAndAssociatedADUsers(string keyWord)
        {
            var groupList = new List<ADGroup>();

            using (DirectoryEntry adserver = new DirectoryEntry(_connector.Path, _connector.User, _connector.PassWord))
            using (DirectorySearcher searcher = new DirectorySearcher(adserver))
            {
                searcher.Filter = String.Concat("(&(objectClass=group)", "(cn=*", keyWord, "*))");
                searcher.SearchScope = SearchScope.Subtree;

                searcher.PropertiesToLoad.Add(MEMBER);

                using (SearchResultCollection results = searcher.FindAll())
                {
                    int i = 0;
                    
                    foreach (SearchResult result in results)
                    {
                        if (i == 20) {
                            Console.WriteLine("This Demo Will Not Process All Groups. It is Limited to 20");
                            break;
                        }

                        var group = GetGroupFromPath(result.Path);

                        Console.WriteLine("Getting User Detail for {0}", group.Name);
                        groupList.Add(group);

                        var members = result.Properties[MEMBER];

                        var users = GetUsersFromMemberList(members);
                        group.Users = new List<ADUser>();
                        group.Users = users;

                        i++;
                    }
                }
            }
            return groupList;
        }

        private List<ADUser> GetUsersFromMemberList(ResultPropertyValueCollection members)
        {
            var users = new List<ADUser>();

            int im = 0;
            
            foreach (string member in members)
            {
                if (im == 50){
                    Console.WriteLine("This Demo Only Processess 50 Users Per Group");
                    break;
                }
                
                var user = new ADUser();

                string[] memSpl = member.Split(',');

                foreach (string pair in memSpl)
                {
                    string[] keyValue = pair.Split('=');

                    if (keyValue[0] == "CN")
                    {
                        user = GetUserFromName(keyValue[1]);
                    }
                }
                users.Add(user);

                im++;
            }
            return users;
        }

        private ADGroup GetGroupFromPath(string path)
        {
            var group = new ADGroup();

            var groupRes = path.Split('/');

            string[] groupSpl = groupRes[3].ToString().Split(',');

            foreach (string pair in groupSpl)
            {
                string[] keyValue = pair.Split('=');

                if (keyValue[0] == "CN")
                {
                    group.Name = keyValue[1];
                    break;
                }
            }
            return group;
        }
    }
}
