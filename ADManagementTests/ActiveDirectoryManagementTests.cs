﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ADManagement;
using ADManagementTests;
using ADModel;
using Microsoft.VisualStudio.TestTools.UnitTesting;
namespace ADManagement.Tests
{
    [TestClass()]
    public class ActiveDirectoryManagementTests
    {
        // a change by DM 

        public static ADConnector connector = new ADConnector(ADConnector.Port.LDAP_CONNECTION, "LDAP://qnrl.com", "daveh", "Passw0rd1");
        public static ActiveDirectoryManagement management = new ActiveDirectoryManagement(connector);


        [TestMethod]
        public void GetUserFromLogin()
        {
            ADUser expected = LocalCreation.CreateUser();

            ADUser user = management.GetUserFromLogin("daveh");

            Assert.AreEqual(expected.DisplayName, user.DisplayName);
            Assert.AreEqual(expected.Login, user.Login);
            Assert.AreEqual(expected.Name, user.Name);
            Assert.AreEqual(expected.PrimaryEmail, user.PrimaryEmail);
            Assert.AreEqual(expected.Surname, user.Surname);

        }

        [TestMethod]
        public void GetUserFromEmail()
        {
            ADUser expected = LocalCreation.CreateUser();

            ADUser user = management.GetUserFromEmail(new System.Net.Mail.MailAddress("dave.hinton@qnrl.com"));

            Assert.AreEqual(expected.DisplayName, user.DisplayName);
            Assert.AreEqual(expected.Login, user.Login);
            Assert.AreEqual(expected.Name, user.Name);
            Assert.AreEqual(expected.PrimaryEmail, user.PrimaryEmail);
            Assert.AreEqual(expected.Surname, user.Surname);
        }

        /// <summary>
        /// Could not get This ex user from name only in the group test
        /// check out why not....
        /// 
        /// The key to this is that the user search used a current\Active user filter. UserAccountControl, now commented out.
        /// 
        /// //   searcher.Filter = "(&(objectClass=user)(objectCategory=person)(!userAccountControl:1.2.840.113556.1.4.803:=2)" + filter + ")";
        /// 
        /// </summary>
        [TestMethod]
        public void CheckExUsers()
        {
            ADUser user = management.GetUserFromEmail(new System.Net.Mail.MailAddress("sarah.lean@qnrl.com"));


            ADUser user2 = management.GetUserFromLogin("SarahL");

            ADUser user3 = management.GetUserFromLogin("ManuelBG");

            ADUser user4 = management.GetUserFromName("Manuel Barroso Gonzalez");

            ADUser user5 = management.GetUserFromName("Sarah Lean");

            // Compare current users
            ADUser user6 = management.GetUserFromName("Dave Hinton");

            ADUser user7 = management.GetUserFromName("David Mateer");
            
            Assert.IsNotNull(user3.Name);  


        }


        [TestMethod]
        public void GetAllUsers()
        {
            List<ADUser> users = management.GetAllUsers();

            var daves = users.Where(u => u.Login == "daveh");

            Assert.IsNotNull(users);
        }


        [TestMethod]
        public void GetAllGroups2()
        {
            List<ADGroup> groups = management.GetAllGroups2();

            var grp = groups.Where(g => g.Name == "Users");

            var da = groups.Where(g => g.Name.Contains("Dom"));

            Assert.IsNotNull(groups);

        }


        [TestMethod]
        public void GetAllGroups()
        {
            List<ADGroup> groups = management.GetAllGroups3();

            var grp = groups.Where(g => g.Name == "Users");

            var da = groups.Where(g => g.Name.Contains("Domain Admins"));

            Assert.IsNotNull(groups);

        }

        [TestMethod]
        public void GetGroupsByName()
        {
            List<ADGroup> groups = management.GetADGroupsAndAssociatedADUsers("Domain Admins");

            Assert.IsNotNull(groups);

        }

        // look at http://stackoverflow.com/questions/15011291/asp-net-active-directory-search ?
        [TestMethod]
        public void GivenASearchForDomainAdminGroup_ShouldReturn50ADUsersInThatGroup () {

            ADGroup domainAdminGroup = management.GetADGroupsAndAssociatedADUsers("Domain Admins").FirstOrDefault();

            Assert.AreEqual(50, domainAdminGroup.Users.Count);
        }

    }
}
