﻿using ADManagement;
using ADModel;

namespace ADManagementTests
{
    public static class LocalCreation
    {
        public static ADUser CreateUser()
        {
            ADUser user = new ADUser();
            user.PrimaryEmail = new System.Net.Mail.MailAddress("dave.hinton@qnrl.com");
            user.Initials = "DNH";
            user.DisplayName = "Dave Hinton";
            user.Name = "Dave";
            user.Surname = "Hinton";
            user.Login = "daveh";
            user.JobTitle = "Application Developer";

            return user;
        }
    }
}
