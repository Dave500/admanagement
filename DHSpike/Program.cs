﻿using System;
using ADPrincipalManagement;
using OutputManagement;

namespace ADPrincipalReporter

{
    class Program
    {
        static void Main()
        {
            string userName = Environment.UserName;
   
            Console.Write("Hi {0}: Enter Group Name or Partial Keyword: ", userName);
            string keyWord = Console.ReadLine();

            Console.WriteLine("Getting Info: Please Wait");

            RunReport(keyWord);

            Console.WriteLine(@"Report Complete: Location C:\temp. GoodBye");

            Console.ReadLine();
        }

        
        private static void RunReport(string keyWord)
        {
            ADPrincipalManagement ad = new ADPrincipalManagement();

            var groups = ad.GetAllGroupsAndTheirUsers(keyWord);

            OutputManager outputMgr = new OutputManager();

            bool reportDone = outputMgr.BuildCsv(groups, keyWord);
        }
    }
}
