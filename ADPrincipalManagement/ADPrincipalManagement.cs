﻿using System;
using System.Collections.Generic;
using System.DirectoryServices.AccountManagement;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ADModel;

namespace ADPrincipalManagement
{
    public class ADPrincipalManager
    {
      
        private PrincipalContext _principalContext;

        public ADPrincipalManager()
        {
           _principalContext = new PrincipalContext(ContextType.Domain, "qnrl.com", null, ContextOptions.Negotiate);
        }


        public ADUser GetUserByName(string name)
        {
            // List Users
            var userPrincipal = new UserPrincipal(_principalContext);
            userPrincipal.Name = "*";
            userPrincipal.DisplayName = name;
            var user = new ADUser();

            var principalSearcher = new PrincipalSearcher();
            principalSearcher.QueryFilter = userPrincipal;

            PrincipalSearchResult<Principal> results = principalSearcher.FindAll();
            //foreach (Principal p in results.OrderBy(x => x.Name))
            foreach (UserPrincipal p in results.OrderBy(x => x.Name))
            {
                user.DisplayName = p.DisplayName;
                if (p.LastLogon != null) user.LastLogin = (DateTime)p.LastLogon;
                user.Login = p.SamAccountName;
            }

            return user;

        }

        public List<ADGroup> GetAllGroupsAndTheirUsers(string name)
        {
            var groups = new List<ADGroup>();
            var groupPrincipal = new GroupPrincipal(_principalContext);
            groupPrincipal.Name = string.Concat("*", name, "*");

            var principalSearcher = new PrincipalSearcher();
            principalSearcher.QueryFilter = groupPrincipal;
            int i = 0;

            PrincipalSearchResult<Principal> results = principalSearcher.FindAll();
            foreach (GroupPrincipal p in results.OrderBy(x => x.Name))
            {
                if (i == 20){
                    Console.WriteLine("This Demo Will Not Process All Groups. It is Limited to 20");
                    break;
                }
                
                Console.WriteLine("Getting User Detail for {0}", p.Name);
                
                ADGroup group = new ADGroup();
                group.Name = p.Name;
                List<ADUser> users = new List<ADUser>();

                int ui = 0;

                foreach (var user in p.Members.OrderBy(x => x.Name))
                {
                    if (ui == 50){
                        Console.WriteLine("This Demo Only Processess 50 Users Per Group");
                        break;
                    }
                    
                    ADUser usera = GetUserByName(user.DisplayName);
                    users.Add(usera);

                    ui++;
                }

                group.Users = users;
                groups.Add(group);

                i++;

            }
            return groups;
        }
    }
}
