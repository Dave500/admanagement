﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using ADModel;
using DocumentFormat.OpenXml.Extensions;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;


namespace OutputManagement
{
    public class OutputManager
    {
        public void SaveStreamToFile(string fileFullPath, Stream stream)
        {
            if (stream.Length == 0) return;

            // Create a FileStream object to write a stream to a file
            using (FileStream fileStream = System.IO.File.Create(fileFullPath, (int)stream.Length))
            {
                // Fill the bytes[] array with the stream data
                byte[] bytesInStream = new byte[stream.Length];
                stream.Read(bytesInStream, 0, (int)bytesInStream.Length);

                // Use FileStream object to write to the specified file
                fileStream.Write(bytesInStream, 0, bytesInStream.Length);
            }
        }

        public bool BuildCsv(List<ADGroup> groups, string keyWord)
        {

            string fromFile = @"c:\temp\ADWorksheet.xlsx";
            string fromFile2 = @"c:\temp\ADWorksheet2.xlsx";
            string toFile = @"c:\temp\ADWorksheet_" + DateTime.Now.Ticks.ToString() + ".xlsx";

            Assembly assembly = Assembly.GetExecutingAssembly();

            using (Stream input = assembly.GetManifestResourceStream("OutputManagement.Resources.ADWorksheet.xlsx"))
            using (Stream output = File.Open(fromFile2, FileMode.Create))
            {
                input.CopyTo(output);
            }

            File.Copy(fromFile2, toFile);

            using (var spreadsheet = SpreadsheetDocument.Open(toFile, true))
            {
                WorkbookPart wbPart = spreadsheet.WorkbookPart;

                WorksheetPart wsPart = GetWorkSheet("ADReport", wbPart);

                AddHeader(wsPart, spreadsheet);

                AddLines(wsPart, spreadsheet, groups);

                wsPart.Worksheet.Save();

                spreadsheet.Close();
            }
           




            return true;
        }

        private static void AddHeader(WorksheetPart wsPart, SpreadsheetDocument spreadsheet)
        {
            WorksheetWriter writer = new WorksheetWriter(spreadsheet, wsPart);
            SpreadsheetStyle style = SpreadsheetStyle.GetDefault(spreadsheet);

            style.SetBackgroundColor("DCE2F0");
            style.SetColor("5B79AB");
            style.IsBold = true;



            string dateFormat = DateTime.Now.ToString();

            writer.PasteText("A1", "", style);
            writer.PasteText("B1", "", style);




            writer.PasteText("C1", "Active Directory Report", style);
            writer.PasteText("D1", "", style);
            writer.PasteText("E1", dateFormat, style);
            writer.PasteText("F1", "", style);
            writer.PasteText("G1", "", style);
            writer.PasteText("H1", "", style);
            writer.PasteText("I1", "", style);
            writer.PasteText("J1", "", style);
            writer.PasteText("K1", "", style);
            writer.PasteText("L1", "", style);
            writer.PasteText("M1", "", style);


        }

        private static void AddLines(WorksheetPart wsPart, SpreadsheetDocument spreadsheet, List<ADGroup> domGroups)
        {

            try
            {
                int colCount = 0;
                string col = "";
                string colRow = "";

                int storeMast = 0;

                WorksheetWriter writer = new WorksheetWriter(spreadsheet, wsPart);
                SpreadsheetStyle style = SpreadsheetStyle.GetDefault(spreadsheet);



                foreach (var group in domGroups)
                {

                    int row = 3;
                    style.IsBold = true;
                    col = GetAlphaColumn(colCount);
                    colRow = col + row.ToString();
                    writer.PasteText(colRow, @group.Name, style);

                    row = 4;
                    colRow = col + row.ToString();
                    writer.PasteText(colRow, "Name", style);


                    colCount++;
                    col = GetAlphaColumn(colCount);
                    colRow = col + row.ToString();
                    writer.PasteText(colRow, "Login", style);


                    colCount++;
                    col = GetAlphaColumn(colCount);
                    colRow = col + row.ToString();
                    writer.PasteText(colRow, "Last Login", style);

                    colCount++;
                    col = GetAlphaColumn(colCount);
                    colRow = col + row.ToString();
                    //Add gap Column


                    colCount = (colCount - 3);
                    row++;
                    style.IsBold = false;

                    if (group.Users != null)
                    {
                        foreach (var user in group.Users)
                        {
                            row++;
                            colRow = col + row.ToString();

                            col = GetAlphaColumn(colCount);
                            colRow = col + row.ToString();
                            writer.PasteText(colRow, @user.DisplayName, style);

                            colCount++;
                            col = GetAlphaColumn(colCount);
                            colRow = col + row.ToString();
                            writer.PasteText(colRow, @user.Login, style);

                            colCount++;
                            col = GetAlphaColumn(colCount);
                            colRow = col + row.ToString();
                            if (user.LastLogin > Convert.ToDateTime("01/01/0001"))
                                writer.PasteText(colRow, @user.LastLogin.ToString(), style);

                            colCount++;
                            col = GetAlphaColumn(colCount);
                            colRow = col + row.ToString();
                            //Add gap Column


                            colCount = (colCount - 3);

                        }


                        colCount = (colCount + 4);
                    }
                }
            }
            catch (Exception a)
            {

                throw a;
            }


        }

        private static WorksheetPart GetWorkSheet(string sheetName, WorkbookPart workBookPart)
        {
            WorksheetPart ws;
            Sheet sheet = workBookPart.Workbook.Descendants<Sheet>().FirstOrDefault(s => s.Name == sheetName);
            ws = (WorksheetPart)(workBookPart.GetPartById(sheet.Id));

            return ws;
        }

        private static string GetAlphaColumn(int colCount)
        {
            string[] cols = { "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z", 
                                "AA", "AB", "AC", "AD", "AE", "AF", "AG", "AH", "AI", "AJ", "AK", "AL", "AM", "AN", "AO", "AP", "AQ", "AR", "AS", "AT", "AU", "AV", "AW", "AX", "AY", "AZ", 
                            "BA", "BB", "BC", "BD", "BE", "BF", "BG", "BH", "BI", "BJ", "BK", "BL", "BM", "BN", "BO", "BP", "BQ", "BR", "BS", "BT", "BU", "BV", "BW", "BX", "BY", "BZ",
                            "CA", "CB", "CC", "CD", "CE", "CF", "CG", "CH", "CI", "CJ", "CK", "CL", "CM", "CN", "CO", "CP", "CQ", "CR", "CS", "CT", "CU", "CV", "CW", "CX", "CY", "CZ",
                            "DA", "DB", "DC", "DD", "DE", "DF", "DG", "DH", "DI", "DJ", "DK", "DL", "DM", "DN", "DO", "DP", "DQ", "DR", "DS", "DT", "DU", "DV", "DW", "DX", "DY", "DZ",
                            "EA", "EB", "EC", "ED", "EE", "EF", "EG", "EH", "EI", "EJ", "EK", "EL", "EM", "EN", "EO", "EP", "EQ", "ER", "ES", "ET", "EU", "EV", "EW", "EX", "EY", "EZ",};

            string col = cols[colCount];

            return col;
        }
    }
}
